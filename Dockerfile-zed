##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM python:3.10.13-slim-bookworm

ARG openstack_release=zed
ARG branch=stable/${openstack_release}

COPY patches/ /tmp

RUN set -eux ; \
    export LANG=C.UTF-8 ; \
    export BUILD_PACKAGES="git pkg-config build-essential" ; \
    export REQUIRED_PACKAGES="curl" ; \
    #
    apt-get update ; \
    apt-get install -y --no-install-recommends ${BUILD_PACKAGES} ${REQUIRED_PACKAGES} ; \
    #
    (git clone https://opendev.org/openstack/requirements.git --depth 1 --branch ${branch} || \
    git clone https://opendev.org/openstack/requirements.git --depth 1 --branch ${openstack_release}-eol || \
    git clone https://opendev.org/openstack/requirements.git --depth 1 --branch unmaintained/${openstack_release}); \
    (git clone https://opendev.org/openstack/barbican.git --depth 1 --branch ${branch} || \
    git clone https://opendev.org/openstack/barbican.git --depth 1 --branch ${openstack_release}-eol || \
    git clone https://opendev.org/openstack/barbican.git --depth 1 --branch unmaintained/${openstack_release}); \
    pip install -U pip ; \
    pip install wheel ; \
    pip install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache python-binary-memcached barbican/ ; \
    #
    echo "oslo.messaging>=$(pip show oslo.messaging | grep Version | awk '{print $2}')" > oslo_messaging_constraint.txt ; \
    pip install -c oslo_messaging_constraint.txt --upgrade oslo.messaging==14.7.0 || true ; \
    #
    patch -d /usr/local/lib/python3.*/site-packages/oslo_messaging -p 2 -i /tmp/fix-rabbitmq-exchange-declare-fallback.patch ; \
    #
    mkdir -p /etc/barbican ; \
    cp barbican/etc/barbican/barbican-api-paste.ini /etc/barbican/ ; \
    #
    addgroup --system barbican >/dev/null ; \
    adduser --system --home /var/lib/barbican --ingroup barbican --shell /bin/false barbican ; \
    chown -R barbican:barbican /var/lib/barbican ; \
    #
    # cleanup BUILD environment
    #
    rm -rf requirements barbican oslo_messaging_constraint.txt ; \
    apt-get purge --auto-remove -y ${BUILD_PACKAGES} ; \
    rm -rf /var/lib/apt/lists/* /root/.cache /var/lib/cache/*

COPY files/barbican-api /bin/

USER barbican

CMD ["/bin/barbican-api"]
